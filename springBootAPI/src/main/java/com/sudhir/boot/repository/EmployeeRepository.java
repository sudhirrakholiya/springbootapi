package com.sudhir.boot.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.sudhir.boot.model.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Long>{

}