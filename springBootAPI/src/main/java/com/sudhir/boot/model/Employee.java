package com.sudhir.boot.model;
import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.GeneratedValue;
import javax.persistence.Entity;

import javax.persistence.GenerationType;


@Entity
@Table(name = "Emp")
public class Employee {

	
	@Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;
	
	@Column(name = "fname", nullable = false)
    private String fname;
	 
	@Column(name = "lname", nullable = false) 
    private String lname;
	
	@Column(name = "email", nullable = false)
    private String email;
 
    public Employee() {
  
    }
 
    public Employee(String fname, String lname, String email) {
         this.fname= fname;
         this.lname= lname;
         this.email= email;
    }
 
    
    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }
   
    public String getFname() {
        return fname;
    }
    public void setfname(String fname) {
        this.fname= fname;
    }
    
    public String getLname() {
        return lname;
    }
    public void setlname(String lname) {
        this.lname= lname;
    }
    
    public String getEmail() {
        return email;
    }
    public void setemail(String email) {
        this.email= email;
    }

    @Override
    public String toString() {
        return "Employee [id=" + id + ", first-Name=" + fname+ ", last-Name=" + lname+ ", email-Id=" + email
       + "]";
    }
 
}