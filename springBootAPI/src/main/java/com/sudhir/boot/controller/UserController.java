package com.sudhir.boot.controller;

import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.TreeMap;

import org.apache.commons.io.comparator.LastModifiedFileComparator;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.sudhir.boot.exception.SpringException;
import com.sudhir.boot.model.Users;
import com.sudhir.boot.service.UserService;


@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/apis")
public class UserController {
	
	static Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static Map<Date, String> mapTree = new HashMap<Date, String>();
	
	@Autowired
	UserService userservices;
	
	@RequestMapping(method = RequestMethod.POST, value = "/saveUser")
	@ExceptionHandler(SpringException.class)
	public String saveUser(@RequestBody Users us) {
		userservices.save(us);
		return new SpringException(true, "Client Sucessfully Added").toString();
	}
	
	@RequestMapping(value = "/getAllUserList", method = RequestMethod.GET, produces = { "application/json" })
	public List<Users> getAllUserList() throws Exception {
		return userservices.getlist();
	}
	
	@RequestMapping(value = "/getUserById/{id}", method = RequestMethod.GET,produces = { "application/json" })
	public Optional<Users> getUserById(@PathVariable long id) {
		return userservices.get(id);
	}
	
	@RequestMapping(method = RequestMethod.POST, value = "/updateUser/{id}")
	@ExceptionHandler(SpringException.class)
	public String updateUser(@RequestBody Users us, @PathVariable int id) {
		us.setId(id);
		return userservices.update(us);
	}
	
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteUser/{id}")
	public String deleteUser(@PathVariable int id) {
		return userservices.delete(id);
	}
	
	@RequestMapping(value = "/getLastAccessedFiles",method = RequestMethod.GET, produces = { "application/json" })
	public String getLastAccessedFiles() throws Exception {
		JSONObject jobj = new JSONObject();
		File currentDir = new File("."); // current directory  
		displayDirectoryContents(currentDir);
		
		int count = 0;
		int max=5;
		Map<Date, String> m1 = new TreeMap<Date, String>(mapTree).descendingMap();
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    for (Map.Entry<Date, String> entry : m1.entrySet()){
	    	if (count >= max) break;
	       // System.out.println("Final:::"+df.format(entry.getKey())+"---------"+entry.getValue());
	        jobj.put(entry.getValue(), df.format(entry.getKey()));
	        count++;
	    }
		return jobj.toString();
	}

	public static void displayDirectoryContents(File dir) throws IOException, ParseException {
		File[] files = dir.listFiles();
		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		for (File file : files) {
			if (file.isDirectory()) {
				displayDirectoryContents(file);
			} else {
				String fileExt =  getFileExtension(file);
				if(fileExt.equals("None")) {
				}else {
					if(file.getName().equals("SpringBootDataJpaApplicationTests.class")) {
					}else {
				    	String strDate = format.format(new Date(file.lastModified()));
						mapTree.put( dateFormat.parse(strDate),file.getName());
				    }
				}
			}
		}
	}
	private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "None";
    }
	
/*	@RequestMapping(value = "/textToSpeech",method = RequestMethod.GET)
	public void textToSpeech() throws Exception {
	    
		String text = "Hello World! How are you doing today? This is Google Cloud Text-to-Speech Demo!";
        String outputAudioFilePath = "C:\\Users\\Sudhir Rakholiya\\Desktop\\voice\\output.mp3";
 
        try (TextToSpeechClient textToSpeechClient = TextToSpeechClient.create()) {
            // Set the text input to be synthesized
            SynthesisInput input = SynthesisInput.newBuilder().setText(text).build();
 
            // Build the voice request; languageCode = "en_us"
            VoiceSelectionParams voice = VoiceSelectionParams.newBuilder().setLanguageCode("en-US")
                    .setSsmlGender(SsmlVoiceGender.FEMALE)
                    .build();
 
            // Select the type of audio file you want returned
            AudioConfig audioConfig = AudioConfig.newBuilder().setAudioEncoding(AudioEncoding.MP3) // MP3 audio.
                    .build();
 
            // Perform the text-to-speech request
            SynthesizeSpeechResponse response = textToSpeechClient.synthesizeSpeech(input, voice, audioConfig);
 
            // Get the audio contents from the response
            ByteString audioContents = response.getAudioContent();
 
            // Write the response to the output file.
            try (OutputStream out = new FileOutputStream(outputAudioFilePath)) {
                out.write(audioContents.toByteArray());
                System.out.println("Audio content written to file \"output.mp3\"");
            }
        }
	}*/
}
