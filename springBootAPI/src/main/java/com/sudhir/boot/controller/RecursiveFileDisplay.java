package com.sudhir.boot.controller;
import java.io.File;
import java.io.IOException;
import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.TreeMap;

import org.apache.commons.io.comparator.LastModifiedFileComparator;

public class RecursiveFileDisplay {
	static Format format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	static Map<Date, String> mapTree = new HashMap<Date, String>();

	public static void main(String[] args) throws Exception {
		File currentDir = new File("."); // current directory  
		displayDirectoryContents(currentDir);
		
		int count = 0;
		int max=5;
		Map<Date, String> m1 = new TreeMap<Date, String>(mapTree).descendingMap();
	    DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	    for (Map.Entry<Date, String> entry : m1.entrySet()){
	    	if (count >= max) break;
	        System.out.println("Final:::"+df.format(entry.getKey())+"--------"+entry.getValue());
	        count++;
	    }
	}

	public static void displayDirectoryContents(File dir) throws IOException, ParseException {
		File[] files = dir.listFiles();
		Arrays.sort(files, LastModifiedFileComparator.LASTMODIFIED_REVERSE);
		for (File file : files) {
			if (file.isDirectory()) {
				displayDirectoryContents(file);
			} else {
				String fileExt =  getFileExtension(file);
				if(fileExt.equals("None")) {
				}else {
					String strDate = format.format(new Date(file.lastModified()));
					mapTree.put( dateFormat.parse(strDate),file.getName()); 
				}
				
			}
		}
	}
	
	private static String getFileExtension(File file) {
        String fileName = file.getName();
        if(fileName.lastIndexOf(".") != -1 && fileName.lastIndexOf(".") != 0)
        return fileName.substring(fileName.lastIndexOf(".")+1);
        else return "None";
    }

}