package com.sudhir.boot.exception;
@SuppressWarnings("serial")
public class SpringException extends RuntimeException
{
	public SpringException(boolean status, String message) {
		this.status = status;
		this.message = message;
	}
	private boolean status;
	private String message;
	public boolean isStatus() {
		return status;
	}
	
	public void setStatus(boolean status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	@Override
	public String toString() {
		return "{\"status\":"+ status +", \"message\":\"" + message + "\"}";
	} 
}
