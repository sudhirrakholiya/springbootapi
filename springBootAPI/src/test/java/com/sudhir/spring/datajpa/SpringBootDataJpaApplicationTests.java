package com.sudhir.spring.datajpa;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.sudhir.boot.SpringBootDataJpaApplication;

@SpringBootTest(classes = SpringBootDataJpaApplication.class)
class SpringBootDataJpaApplicationTests {

	@Test
	void contextLoads() {
	}

}
